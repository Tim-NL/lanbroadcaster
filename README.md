LANBroadcaster
==============

Bukkit plugin to broadcast a Minecraft server over LAN.

![Console](http://i.imgur.com/TBpVM.png)

![Server list](http://i.imgur.com/S3MF2.png)
